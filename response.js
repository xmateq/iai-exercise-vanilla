const response = {
  "product": {
    "id": 1,
    "name": "Konsola MICROSOFT XBOX ONE S 500GB + FIFA 19",
    "code": "10000053",
    "icon": "\/data\/gfx\/icons\/large\/7\/0\/7.jpg",
    "link": "\/pl\/products\/xbox-4-slim-7.html",
    "product_type": "product_item"
  },
  "sizes": {
    "unit": "szt.",
    "unit_single": "szt.",
    "unit_plural": "szt.",
    "unit_fraction": "sztuka",
    "unit_precision": "0",
    "unit_sellby": 1,
    "items": {
      "U": {
        "type": "U",
        "name": "Ram 32 GB",
        "amount": 1,
        "status": "Produkt dostępny",
        "price": 125.00
      },
      "V": {
        "type": "V",
        "name": "Ram 64 GB",
        "amount": 12,
        "status": "Produkt dostępny",
        "price": 159.00
      },
      "W": {
        "type": "W",
        "name": "Ram 128 GB",
        "amount": 0,
        "status": "Produkt niedostępny",
        "price": 199.00
        }
      }
  },
  "multiversions": [
    {
      "id": "1",
      "name": "Wariant",
      "items": {
        "1-1": {
          "enabled": true,
          "selected": true,
          "products": [
            {
              "product_id": 7,
              "version_priority": "1",
              "url": "\/pl\/products\/xbox-7.html",
              "price_difference": "0.00"
            }
          ],
          "values": {
            "61": {
              "id": 61,
              "name": "Srebrny"
            }
          },
          "values_id": "61"
        },
        "1-2": {
          "enabled": true,
          "products": [
            {
              "product_id": 8,
              "version_priority": "2",
              "url": "\/pl\/products\/xbox-4-slim-8.html",
              "price_difference": "-5.00"
            }
          ],
          "values": {
            "60": {
              "id": 60,
              "name": "Czarny"
            }
          },
          "values_id": "60"
        },
        "1-3": {
          "enabled": true,
          "products": [
            {
              "product_id": 27,
              "version_priority": "2",
              "url": "\/pl\/products\/xbox-4-slim-27.html",
              "price_difference": "-10.00"
            }
          ],
          "values": {
            "59": {
              "id": 59,
              "name": "Biały"
            }
          },
          "values_id": "59"
        }
      }
    }
  ]
}

const modal = document.getElementById("modal");

const btn = document.getElementById("btn");

const closeButton = document.getElementById("modal-close");

const dropdownButton = document.querySelector(".modal-variant-container");

const dropdown = document.getElementById("modal-dropdown");

btn.onclick = function() {
  modal.style.visibility = "visible";
}

closeButton.onclick = function() {
  modal.style.visibility = "hidden";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.visibility = "hidden";
  }
}

dropdownButton.onclick = function() {
  if(dropdown.style.display == "block") {
    dropdown.style.display = "none";
  } else {
    dropdown.style.display = "block";
  }
}

document.querySelector(".modal-title").innerHTML = response.product.name;

const price = document.querySelector(".modal-price");
price.innerHTML = response.sizes.items.U.price.toFixed(2) + " zł";

const image = document.querySelector(".modal-image-img");
const colorChangeIcon = document.querySelector(".dropdown-variant-icon");

const sizeButton1 = document.querySelector(".modal-size-buttons-btn-1");
const sizeButton2 = document.querySelector(".modal-size-buttons-btn-2");
const sizeButton3 = document.querySelector(".modal-size-buttons-btn-3");
sizeButton1.innerHTML = response.sizes.items.U.name;
sizeButton2.innerHTML = response.sizes.items.V.name;
sizeButton3.innerHTML = response.sizes.items.W.name;

const listElement1 = document.querySelector(".dropdown-element-1");
const listElement2 = document.querySelector(".dropdown-element-2");
const listElement3 = document.querySelector(".dropdown-element-3");
listElement1.innerHTML = response.multiversions[0].items["1-1"].values[61].name;
listElement2.innerHTML = response.multiversions[0].items["1-2"].values[60].name;
listElement3.innerHTML = response.multiversions[0].items["1-3"].values[59].name;

const available = document.getElementById("modal-product-available-status");
const availableIcon = document.querySelector(".check-icon");
const unavailable = document.getElementById("modal-product-unavailable-status");
const unavailableIcon = document.querySelector(".cross-icon");

available.innerHTML = response.sizes.items.U.status;
unavailable.innerHTML = response.sizes.items.W.status;

const colorChange = document.getElementById("modal-variant-btn");
colorChange.innerHTML = response.multiversions[0].items["1-1"].values[61].name;

sizeButton1.onclick = () => {
  price.innerHTML = response.sizes.items.U.price.toFixed(2) + " zł";
  if (response.sizes.items.U.amount > 0) {
    unavailable.style.display = "none";
    unavailableIcon.style.display = "none";
    available.style.display = "block";
    availableIcon.style.display = "block";
  } else {
    unavailable.style.display = "block";
    unavailableIcon.style.display = "block";
    available.style.display = "none";
    availableIcon.style.display = "none";
  }
  price.innerHTML = response.sizes.items.U.price.toFixed(2) + " zł";
}

sizeButton2.onclick = () => {
  price.innerHTML = response.sizes.items.V.price.toFixed(2) + " zł";
  if (response.sizes.items.V.amount > 0) {
    unavailable.style.display = "none";
    unavailableIcon.style.display = "none";
    available.style.display = "block";
    availableIcon.style.display = "block";
  } else {
    unavailable.style.display = "block";
    unavailableIcon.style.display = "block";
    available.style.display = "none";
    availableIcon.style.display = "none";
  }
  price.innerHTML = response.sizes.items.V.price.toFixed(2) + " zł";
}

sizeButton3.onclick = () => {
  price.innerHTML = response.sizes.items.W.price.toFixed(2) + " zł";
  if (response.sizes.items.W.amount > 0) {
    unavailable.style.display = "none";
    unavailableIcon.style.display = "none";
    available.style.display = "block";
    availableIcon.style.display = "block";
  } else {
    unavailable.style.display = "block";
    unavailableIcon.style.display = "block";
    available.style.display = "none";
    availableIcon.style.display = "none";
  }
  price.innerHTML = response.sizes.items.W.price.toFixed(2) + " zł";
}

listElement1.onclick = () => {
  colorChange.innerHTML = response.multiversions[0].items["1-1"].values[61].name;
  if(dropdown.style.display == "block") {
    dropdown.style.display = "none";
  }
  image.src = "https://www.skinit.com/media/catalog/product/cache/9dbe6a0c16a5b581719a1aa389879cfc/p/r/product_s_i_silver-carbon-fiber-xbox-one-s-console-and-controller-bundle-skin-1517596148_skntxtcbfraxbx1sb-pr-01_80.jpg"
}

listElement2.onclick = () => {
  colorChange.innerHTML = response.multiversions[0].items["1-2"].values[60].name;
  if(dropdown.style.display == "block") {
    dropdown.style.display = "none";
  }
  image.src = "https://image.ceneostatic.pl/data/products/30669861/d252718e-9204-4df1-92b8-4e207f16354d_i-microsoft-xbox-one-500gb-czarny.jpg"
}

listElement3.onclick = () => {
  colorChange.innerHTML = response.multiversions[0].items["1-3"].values[59].name;
  if(dropdown.style.display == "block") {
    dropdown.style.display = "none";
  }
  image.src = "https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RW2OuB?ver=2804"
}
